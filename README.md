#To build
npm install gulp-cli -g
npm install gulp -D
gulp

#To Run
npm install -g nodemon
nodemon server.js

#To Debug
npm install -g node-inspector
node-debug server.js

#To Deploy to openshift
git push alt master