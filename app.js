var express = require('express');
var compression = require('compression');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var methodOverride = require('method-override')
var release = require('./modules/api/release');
var top10 = require('./modules/api/top-10');
var authentication = require('./modules/api/authentication');
var discogs = require('./modules/api/discogs');

var session = require('express-session');
var MongoStore = require('connect-mongo/es5')(session);

var mongoClient = require('mongodb').MongoClient;
var assert = require('assert');

var passport = require('passport');
var oauth = require('./modules/oauth');
var user = require('./modules/user');
var mongoInit = require('./modules/mongo-init');
var secretToken = process.env.OPENSHIFT_SECRET_TOKEN || "mySecretToken";
var FacebookStrategy = require('passport-facebook').Strategy;

var oneDay = 86400000;

var port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080;
var ip = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";

//------------------------------------------------------------------
// Configure passport
//------------------------------------------------------------------
passport.serializeUser(function(user, done) {
    done(null, user);
});
passport.deserializeUser(function(obj, done) {
    done(null, obj);
});
console.log(JSON.stringify(oauth));


//------------------------------------------------------------------
// Connect to mongo
//------------------------------------------------------------------
var mongoUrl;

if (process.env.OPENSHIFT_MONGODB_DB_HOST) {
    mongoUrl = 'mongodb://admin:9V13PzNalc1g@' + process.env.OPENSHIFT_MONGODB_DB_HOST + ':' + process.env.OPENSHIFT_MONGODB_DB_PORT + '/katalogo';
} else {
    mongoUrl = 'mongodb://localhost:27017/katalogo';
}

console.log("Mongo URL is " + mongoUrl);

mongoClient.connect(mongoUrl, function(err, db) {
    assert.equal(null, err);
    console.log("Connected correctly to server. " + db);
    mongoInit.initialise(db);

    passport.use(new FacebookStrategy({
            clientID: oauth.facebook.clientID,
            clientSecret: oauth.facebook.clientSecret,
            callbackURL: oauth.facebook.callbackURL,
            profileFields: ['id', 'displayName', 'name', 'gender', 'picture.type(large)']
        },
        function(accessToken, refreshToken, profile, done) {
            process.nextTick(function() {
                user.getLocalUser(profile, 'facebook', db, function (localUser) {
                    if(!localUser) {
                        done('dberror');
                    }
                    return done(null, localUser);
                });
            });
        }
    ));

    //------------------------------------------------------------------
    // Initialise API
    //------------------------------------------------------------------
    release.initialise(db);
    top10.initialise(db);

    //------------------------------------------------------------------
    // Configure Express
    //------------------------------------------------------------------
    var app = express();
    app.use(compression());
    app.use(express.static(__dirname + '/public', {
        maxAge: oneDay
    }));
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
    app.use(cookieParser(secretToken));
    app.use(methodOverride());
    app.use(session({
        secret: secretToken,
        resave: false,
        saveUninitialized: false,
        store: new MongoStore({
            db: db,
            ttl: 60 * 30 //30 minutes
        })
    }));
    app.use(express.static(__dirname + '/public'));
    app.use(passport.initialize());
    app.use(passport.session());

    var router = express.Router();

    // log all requests
    router.use(function(req, res, next) {
        console.log('Request ' + req.originalUrl);
        next(); // make sure we go to the next routes and don't stop here
    });

    oauth.addRoutes(router, db);
    authentication.addRoutes(router);
    release.addRoutes(router);
    top10.addRoutes(router);
    discogs.addRoutes(router);

    app.use(router);
    app.listen(port, ip, function() {
        console.log("Listening on " + ip + ", port " + port)
    });
});
