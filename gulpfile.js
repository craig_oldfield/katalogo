/*
!!README!!
if you get this really unhelpful error message when doing a build,

    Error: File.contents can only be a Buffer, a Stream, or null.

The most likely cause is that one of your modules has a require statement that is requiring a module (or html template)
that doesn't exist
*/
var gulp = require('gulp');
var browserify = require('browserify');
var browserifyCss = require('browserify-css');
var through2 = require('through2');

var hbsfy = require('hbsfy').configure({
  extensions: ["html"]
});

gulp.task('scripts', function() {

    gulp.src('modules/app/index.js')
        .pipe(through2.obj(function (file, enc, next){
            console.log(file.path);
            browserify(file.path)
                .transform(browserifyCss, {
                    processRelativeUrl: function(relativeUrl) {
                        var stripQueryStringAndHashFromPath = function(url) {
                            return url.split('?')[0].split('#')[0];
                        };
                        var relativePath = stripQueryStringAndHashFromPath(relativeUrl);
                        var queryStringAndHash = relativeUrl.substring(relativePath.length);

                        //
                        // Copying files from '../node_modules/bootstrap/' to 'dist/vendor/bootstrap/'
                        //
                        var prefix = 'node_modules/bootstrap/dist/';
                        if (relativePath.startsWith(prefix)) {
                            var clientPath = relativePath.substring(prefix.length);
                            return clientPath + queryStringAndHash;
                        }

                        return relativeUrl;
                    }
                })
                .transform(hbsfy)
                .bundle(function(err, res){
                    if(err) console.log(err);
                    file.contents = res;
                    next(null, file);
                });
        }))
        .pipe(gulp.dest('public/js/'));
});

gulp.task('fonts', function() {
    gulp.src("node_modules/bootstrap/dist/fonts/*").pipe(gulp.dest("public/fonts/"));
});

gulp.task('default', ['scripts', 'fonts']);