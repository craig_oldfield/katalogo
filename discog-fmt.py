#!/usr/bin/python
from __future__ import print_function

import gzip
import json
import sys
import codecs
import re
import os


from xml.dom.minidom import parseString
import xml.dom.minidom

if len(sys.argv) < 2 :
    print("Usage: discog-fmt.py filename")
    print("E.g. discog-fmt.py discogs_20170501_releases.xml.gz")
    sys.exit()

gzippedFilename = sys.argv[1]
extractedFilename = gzippedFilename[:-3]
print("Extracting " + gzippedFilename + " to " + extractedFilename)

gzippedFile = gzip.open(gzippedFilename, 'rb')
extractedFile = open(extractedFilename, 'wb')
extractedFile.write( gzippedFile.read() )
gzippedFile.close()
extractedFile.close()

bytesToProcess = os.path.getsize(extractedFilename)
bytesProcessed = 0
#print "Total files size {} bytes".format(bytesToProcess)

formattedFilename = 'katalogo-release.json'

print("Reformatting release data, writing to file  " + formattedFilename)

regex = re.compile(r"<notes>.*</notes>", re.IGNORECASE)

extractedFile = codecs.open(extractedFilename, "r", "utf-8")
formattedFile = codecs.open(formattedFilename, "w", "utf-8")
release = ""
errorCount = 0
readCount = 0
writeCount = 0
for line in extractedFile :
    bytesProcessed += len(line)
    release += line.replace('\n', ' ').replace('\r', '')
    if "</release>" in release :
        readCount += 1
        release = regex.sub("", release)
        try:
            DOMTree = parseString(release)
        except xml.parsers.expat.ExpatError as e:
            print("Expat error({0}): line {1} char {2}".format(e.code, e.lineno, e.offset))
            errorCount += 1
            release = ""
            continue
        collection = DOMTree.documentElement

        labels = collection.getElementsByTagName("label")
        if(len(labels) > 0) :
            catno = labels[0].getAttribute("catno").replace(' ', '').replace('-', '').lower()
            if catno == "none" or catno == "" :
                release = ""
                continue

            title = collection.getElementsByTagName("title")[0].childNodes[0].data
            formats = collection.getElementsByTagName("format")

            released = collection.getElementsByTagName("released")
            if(len(released) > 0) :
                year = released[0].childNodes[0].data[:4]
            else:
                year = ''

            styles = collection.getElementsByTagName("style")
            if(len(styles) > 0) :
                genre = styles[0].childNodes[0].data
            else:
                genre = ''


            releaseJSON = {
                'catno': catno,
                'title': title,
                'genre': genre,
                'year': year,
                'artist': collection.getElementsByTagName("artist")[0].getElementsByTagName("name")[0].childNodes[0].data
            }
            formattedFile.write(json.dumps(releaseJSON) + "\n");
            writeCount += 1

        release = ""
        if readCount % 1000 == 0 or (errorCount > 0 and errorCount % 100 == 0):
            print("\rRead {:,} converted {:,} errors {:,} : {}% complete".format(readCount, writeCount, errorCount, int(100 * float(bytesProcessed) / bytesToProcess)), end='\r')
print("\rRead {:,} converted {:,} errors {:,} : 100% complete".format(readCount, writeCount, errorCount), end='\n')
print("Done.")
extractedFile.close()
