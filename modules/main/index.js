module.exports = (function() {

    var template = require('./main.html');
    var editRelease = require('../edit-release');
    var listRelease = require('../list-release');
    var top10Genre = require('../top-10');
    var top10Artist = require('../top-10');
    var top10Format = require('../top-10');

    var initialise = function(parentNode, user) {
        parentNode.append(template());

        $.subscribe("release.new", function() {
            $('#content').html('');
            editRelease.initialise($('#content'));
        });

        $.subscribe("release.edit", function(evt, releaseId) {
            $('#content').html('');
            editRelease.initialise($('#content'), releaseId);
        });

        $.subscribe('release.created', function(evt, release) {
            console.log("release created, createAnother=" + release.createAnother);
            $('#content').html('');
            if (release.createAnother) {
                editRelease.initialise($('#content'), null, release.createAnother);
            } else {
                listRelease.initialise($('#content'));
                _refreshTop10();
            }
        });

        $.subscribe('release.updated', function(evt, release) {
            $('#content').html('');
            listRelease.initialise($('#content'));
            _refreshTop10();
        });

        listRelease.initialise($('#content'));
        _refreshTop10();
    };

    var _refreshTop10 = function () {
        top10Genre.initialise($('#genre-top-10'), 'Top Genres', 'genre');
        top10Format.initialise($('#format-top-10'), 'Top Years', 'year');
        top10Artist.initialise($('#artist-top-10'), 'Top Artists', 'artist');
    }

    return {
        initialise: initialise
    };

})();
