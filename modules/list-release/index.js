module.exports = (function() {

    var layoutTemplate = require('./layout.html');
    var filterTemplate = require('./filter.html');
    var releaseTemplate = require('./releases.html');
    var parentNode;

    var filter = {
        filterBy: '',
        keyword: ''
    }

    var initialise = function(node) {
        parentNode = node;

        var layout = layoutTemplate();

        parentNode.html(layout);

        $('#list-release-filter').html(filterTemplate({filter: filter}));
        $('#filterBy').on('change', _doFilter);
        $('#filterKeyword').on('keyup', _doFilter);

        _doFilter();

        $('#actionsModal').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var releaseId = button.data('release-id'); // Extract info from data-* attributes
            var artist = button.data('artist'); // Extract info from data-* attributes
            var title = button.data('title'); // Extract info from data-* attributes
            var modal = $(this);

            modal.find('.modal-title').text(artist + ' - ' +
                title);
            $('#edit-release').off();
            $('#edit-release').on('click', function() {
                $('#actionsModal').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $.publish('release.edit', releaseId);
            });

            $('#delete-release').off();
            $('#delete-release').on('click', function() {
                $('#actionsModal').modal('hide');
                $.ajax({
                    url: '/api/release/' + releaseId,
                    method: 'DELETE'
                })
                .done(function(release) {
                    _doFilter();
                })
                .fail(function(error) {
                    alert('error ' + JSON.stringify(error));
                });
            });
        });

    };

    var _handleSuccess = function(data) {

        $('#list-release-releases').html(releaseTemplate({
            filter: filter,
            releases: data
        }));
        $('.delete-button').on('click', function(event) {
            $.ajax({
                url: '/api/release/' + $(event.currentTarget).data('release-id'),
                method: 'DELETE'
            })
            .done(function(release) {
                _doFilter();
            })
            .fail(function(error) {
                alert('error ' + JSON.stringify(error));
            });
        });
        $('.edit-button').on('click', function(event) {
            $.publish('release.edit', $(event.currentTarget).data('release-id'));
        });
    }

    var _handleFail = function(data) {
        alert(JSON.stringify(data));
    }

    var _doFilter = function() {
        filter.filterBy = $('#filterBy').val();
        filter.keyword = $('#filterKeyword').val();
        if(filter.keyword.length > 2) {
            $.get({
                url: '/api/release',
                data: filter
            })
            .done(_handleSuccess)
            .fail(_handleFail);
        }
    }

    return {
        initialise: initialise
    };

})();
