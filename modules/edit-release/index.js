module.exports = (function() {

    var template = require('./edit-release.html');

    var initialise = function(parentNode, releaseId, createAnother) {

        if (releaseId) {
            $.get('api/release/' + releaseId)
                .done(function(data) {
                    data.pageTitle = "Edit Release";
                    parentNode.append(template(data));
                    $('#update-release').show();
                    $('#update-release').on('click',
                        _updateRelease);
                });
        } else {
            parentNode.append(template({
                pageTitle: 'New Release'
            }));
            $('#create-release').show();
            $('#create-another-container').css('display', 'inline-block');
            if (createAnother) {
                $('#create-another').prop('checked', true);
            }
            $('#create-release').on('click', _createRelease);
            $("#release-image-picker").imagepicker();
        }

        $('#search-release').on('click', function() {
            var catno = $('#catalog-number').val().replace(/\s/g, '');

            var populateForm = function (remoteRelease) {
                $('#artist').val(remoteRelease.artist);
                $('#title').val(remoteRelease.title);
                $('#year').val(remoteRelease.year);
                $('#genre').val(remoteRelease.genre);

                $('#release-image-picker').empty();
                if (remoteRelease && remoteRelease.images) {
                    for (var i = 0; i< remoteRelease.images.length; i++) {
                        $('#release-image-picker').append('<option data-img-src="'+remoteRelease.images[i]+'" data-img-alt="Image '+i+'" value="'+i+'"> Image '+i+'</option>');
                    }
                }
                $("#release-image-picker").imagepicker();

                if(remoteRelease.type == 'Album') {
                    $('#album-radio').prop('checked', true);
                } else if (remoteRelease.type == 'Single') {
                    $('#single-radio').prop('checked', true);
                } else if (remoteRelease.type == 'EP') {
                    $('#ep-radio').prop('checked', true);
                } else if (remoteRelease.type == 'Compilation') {
                    $('#compilation-radio').prop('checked', true);
                }

                if(remoteRelease.format == '12" Vinyl') {
                    $('#vinyl12-radio').prop('checked', true);
                } else if(remoteRelease.format == '10" Vinyl') {
                    $('#vinyl10-radio').prop('checked', true);
                } else if(remoteRelease.format == '7" Vinyl') {
                    $('#vinyl7-radio').prop('checked', true);
                } else if(remoteRelease.format == 'CD') {
                    $('#cd-radio').prop('checked', true);
                }
            };

            _getDiscogsRelease(catno, populateForm, function() {});
        });
    };

    var _getMusicbrainzRelease = function(catno, foundCallback, notFoundCallback) {

        $.get('http://musicbrainz.org/ws/2/release/?query=catno:' + catno + '&fmt=json')
            .done(function(data) {
                if(data && data.releases && data.releases.length > 0) {
                    var release = data.releases[0];
                    var releaseGroup = release['release-group'];
                    var mediumList = release['medium'];

                    var response = {};
                    response.artist = release['artist-credit'][0].artist.name;
                    response.title = release.title;
                    response.year = release.date.substring(0,4);
                    if (releaseGroup) {
                        response.type = releaseGroup['primary-type'];
                    }
                    if(release.media && release.media.length > 0) {
                        response.format = release.media[0].format;
                    }
                    foundCallback(response);
                } else {
                    notFoundCallback();
                }
            }).fail(function () {
                notFoundCallback();
            });

    }

    var _getDiscogsRelease = function(catno, foundCallback, notFoundCallback) {
        $.get('/api/discogs/' + catno)
            .done(function(remoteRelease) {
                if(remoteRelease) {
                    foundCallback(remoteRelease);
                } else {
                    notFoundCallback();
                }
            }).fail(function () {
                notFoundCallback();
            });
    }

    var _createRelease = function() {
        $.post({
                url: '/api/release',
                data: {
                    catalogNumber: $('#catalog-number').val(),
                    artist: $('#artist').val(),
                    title: $('#title').val(),
                    year: $('#year').val(),
                    genre: $('#genre').val(),
                    type: $('input[name=type]:checked').val(),
                    format: $('input[name=format]:checked').val(),
                    imageurl: $('#release-image-picker').find(':selected').data('img-src')
                }
            })
            .done(function(release) {
                release.createAnother = $("#create-another").is(':checked');
                $.publish('release.created', release);
            })
            .fail(function(error) {
                alert('error ' + JSON.stringify(error));
            });
    }

    var _updateRelease = function() {
        $.ajax({
                url: '/api/release',
                method: 'PUT',
                data: {
                    _id: $('#release-id').val(),
                    catalogNumber: $('#catalog-number').val(),
                    artist: $('#artist').val(),
                    title: $('#title').val(),
                    year: $('#year').val(),
                    genre: $('#genre').val(),
                    type: $('input[name=type]:checked').val(),
                    format: $('input[name=format]:checked').val(),
                    imageurl: $('#release-image-picker').find(':selected').data('img-src')
                }
            })
            .done(function(release) {
                $.publish('release.updated', release);
            })
            .fail(function(error) {
                alert('error ' + JSON.stringify(error));
            });
    }

    return {
        initialise: initialise
    };

})();
