module.exports = (function() {

    var mongo = require('mongodb');

    var COLLECTION = "user",
        db,
        error,
        result;

    var initialise = function(mongo) {
        db = mongo;
    }

    var getLocalUser = function(profile, provider, db, callback) {
        var localUser = {
            photos: profile.photos,
            displayName: profile.displayName
        }, query = {};
        query[provider] = profile.id;
        console.log('looking for ' + JSON.stringify(query));
        db.collection(COLLECTION).findOne(query, function(err, existingUser) {
            if (err) {
                console.log(JSON.stringify(err));
                callback(null);
            } else if (existingUser) {
                console.log('found  ' + JSON.stringify(existingUser));
                existingUser.photos = localUser.photos;
                existingUser.displayName = localUser.displayName;
                callback(existingUser);
            } else {
                console.log('user not found, creating')
                localUser[provider] = profile.id;
                result = db.collection(COLLECTION).insert(localUser);
                console.log('created  ' + JSON.stringify(localUser));
                callback(_processWriteResult(result, localUser));
            }
        });
    }

    var _processWriteResult = function(writeResult, localUser) {
        var error;
        if (result.writeConcernError) error = result.writeConcernError;
        if (result.writeError) error = result.writeError;
        if (error) {
            console.log('ERROR - ' + JSON.stringify(error));
            return null;
        }
        console.log('returning  ' + JSON.stringify(localUser));
        return localUser;
    }

    return {
        initialise: initialise,
        getLocalUser: getLocalUser

    };

})();
