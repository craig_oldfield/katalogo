module.exports = (function() {

    //rhc env set FACEBOOK_CLIENT_ID=1873656392847183 FACEBOOK_CLIENT_SECRET=4d9710b119c7b13990b3abc777754aec -a katalogo


    var passport = require('passport');
    var user = require('../user')

    var dns = process.env.OPENSHIFT_APP_DNS || "localhost:8080";
    var url = 'http://' + dns;

    var addRoutes = function(router, db) {
        router.route('/')
            .get(function(req, res) {
                res.redirect('/login.html');
            });

        router.route('/auth/facebook')
            .get(passport.authenticate('facebook'), function(req, res) {});

        router.route('/auth/facebook/callback')
            .get(passport.authenticate('facebook', {
                    failureRedirect: '/'
                }),
                function(req, res) {
                    res.redirect('/');
                });

        router.route('/logout')
            .get(function(req, res) {
                req.logout();
                res.redirect('/');
            });
    }

    var ensureAuthenticated = function(req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }
        res.redirect('/');
    }

    return {
        addRoutes: addRoutes,
        ensureAuthenticated: ensureAuthenticated,
        facebook: {
            clientID: process.env.FACEBOOK_CLIENT_ID || '145903125934243',
            clientSecret: process.env.FACEBOOK_CLIENT_SECRET || '38319c1eb902f3319b4d9f5573d9844d',
            callbackURL: url + '/auth/facebook/callback'
        }
    };

})();
