module.exports = (function () {

	var template = require('./primary-btn.html');

	var initialise = function (parentNode, text, icon, topic) {

		var btn = $('<div/>').html(template({
			text: text,
			icon: icon
		})).contents();

		btn.on("click", function() {
			$.publish(topic);
		})

		parentNode.append(btn);
	};

	return {
		initialise: initialise
	};

})();