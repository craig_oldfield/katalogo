module.exports = (function() {

    var mongo = require('mongodb');

    var RELEASE_COLLECTION = "release",
        db,
        error,
        result;

    var initialise = function(mongo) {
        db = mongo;
        console.log("Ensuring userId index exists on release");
        db.collection(RELEASE_COLLECTION).ensureIndex({userId:1});

        console.log("Ensuring artist, title index exists on release");
        db.collection(RELEASE_COLLECTION).ensureIndex({artist:1, title:1});
    }

    return {
        initialise: initialise
    }
})();
