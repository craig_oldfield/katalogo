var jQuery = require('jquery');
window.$ = window.jQuery = jQuery;

require('bootstrap');
require('../pubsub');
require('./image-picker-min.js');

(function() {
    var css = require('./app.css');
    var main = require('../main');
    var login = require('../login');

    var Handlebars = require("hbsfy/runtime");

    Handlebars.registerHelper("setChecked", function(value, currentValue) {
        if (value == currentValue) {
            return "checked"
        } else {
            return "";
        }
    });

    Handlebars.registerHelper("setSelected", function(value, currentValue) {
        if (value == currentValue) {
            return "selected"
        } else {
            return "";
        }
    });

    $(document).ready(function() {

        var template = require('./app.html');
        var header = require('../header');

        $.get('api/auth').done(function(user) {
            $('#app').html(template());
            if (user && user.isAuthenticated) {
                header.initialise($('#headerContainer'), user);
                main.initialise($('#mainContainer'), user);
            } else {
                login.initialise($('#mainContainer'));
            }

        });
    });
})();
