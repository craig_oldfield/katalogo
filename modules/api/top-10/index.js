module.exports = (function() {

    var mongo = require('mongodb');
    var oauth = require('../../oauth');

    var COLLECTION = "release",
        db,
        error,
        result;

    var initialise = function(mongo) {
        db = mongo;
    }

    var _list = function(req, res) {

        var match = {userId: req.user._id};
        match[req.params.field] = {$ne: "Various"};

        db.collection(COLLECTION).aggregate([
            {$match: match},
            {$group: {_id: "$"+req.params.field, count: {$sum: 1}}},
            {$sort: {count: -1, _id: 1}},
            {$limit: 10}
        ],
        function(err, top10) {
            if (err) {
                console.log(JSON.stringify(err));
                res.status(500).json(err);
            } else {
                console.log(JSON.stringify(top10));
                res.status(200).json(top10);
            }
        });
    }

    var addRoutes = function(router) {
        router.route('/api/top-10/:field')
            .all(oauth.ensureAuthenticated)
            .get(function(req, res) {
                _list(req, res);
            });
    }

    return {
        addRoutes: addRoutes,
        initialise: initialise
    }


})();
