module.exports = (function() {

    var oauth = require('../../oauth');

    var get = function(req, res) {
        var user = {};
        if(req.isAuthenticated()) {
            user = req.user;
            user.profileImage = req.user.photos.length > 0 ? req.user.photos[0].value : "default_user.png";
        }
        user.isAuthenticated = req.isAuthenticated();
        res.status(200).json(user);
    }

    var addRoutes = function(router) {
        router.route('/api/auth')
            .get(function(req, res) {
                get(req, res);
            });
    }

    return {
        get: get,
        addRoutes: addRoutes
    }

})();
