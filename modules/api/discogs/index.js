module.exports = (function() {

    var https = require('https');
    var path = require('path');
    var appDir = path.dirname(require.main.filename);
    var appPackage = require(appDir + '/package.json');

    var oauth = require('../../oauth');

    var key = 'fyBRYRtgwzYfWgtqlGjV';
    var secret = 'DbXpigeUzfGvRgpMueWHImOaCZgLsKGW';
    var authParam = 'key=fyBRYRtgwzYfWgtqlGjV&secret=DbXpigeUzfGvRgpMueWHImOaCZgLsKGW';
    var searchPath = '/database/search?page=1&per_page=1&catno=';
    var releasePath = '/releases/';
    var discogsHost = 'api.discogs.com';

    var get = function(req, res) {

        const options = {
            hostname: discogsHost,
            path: searchPath + req.params.catno + '&' + authParam,
            method: 'GET',
            headers: {
                'User-Agent': 'Katalogo/' + appPackage.version
            }
        };

        var searchResults;

        var searchCallback = function(httpsResponse) {
            var responseBody = '';
            httpsResponse.on('data', function(chunk) {
                responseBody += chunk;
            });
            httpsResponse.on('end', function() {
                searchResults = JSON.parse(responseBody);
                if(searchResults.results.length > 0) {
                    var result = searchResults.results[0];
                    getRelease(res, result.id);
                } else {
                    res.status(200).json({});
                }

            });
        };

        console.log('Sending GET request to https://' + options.hostname + options.path);

        var searchRequest = https.request(options, searchCallback);

        searchRequest.on('error', function(err) {
            console.log(JSON.stringify(err));
            res.status(500).send();
        });

        searchRequest.end();
    }

    var getRelease = function(res, id) {

        var options = {
            hostname: discogsHost,
            path: releasePath + id + '?' + authParam,
            method: 'GET',
            headers: {
                'User-Agent': 'Katalogo/' + appPackage.version
            }
        };

        var response;

        var httpsCallback = function(httpsResponse) {
            var responseBody = '';
            httpsResponse.on('data', function(chunk) {
                responseBody += chunk;
            });
            httpsResponse.on('end', function() {
                response = JSON.parse(responseBody);
                var release = {};

                if(response) {
                    if(response.styles && response.styles.length > 0) {
                        release.genre = response.styles[0];
                    }
                    if(response.year) {
                        release.year = response.year
                    }
                    if(response.title) {
                        release.title = response.title
                    }
                    if(response.artists && response.artists.length > 0) {
                        for (var i = 0; i < response.artists.length; i++) {
                            if (release.artist) {
                                artist = artist + ' ' + response.artists[i].join + ' ' + response.artists[i].name;
                            } else {
                                release.artist = response.artists[i].name;
                            }
                        }
                    }
                    if(response.images && response.images.length > 0) {
                        release.images = [];
                        for (var i = 0; i < response.images.length; i++) {
                            release.images.push(response.images[i].uri);
                        }
                    }
                }
                res.status(200).json(release);
            });
        };

        console.log('Sending GET request to https://' + options.hostname + options.path);

        var httpsRequest = https.request(options, httpsCallback);

        httpsRequest.on('error', function(err) {
            console.log(JSON.stringify(err));
            res.status(500).send();
        });

        httpsRequest.end();
    }

    var addRoutes = function(router) {
        router.route('/api/discogs/:catno')
            .all(oauth.ensureAuthenticated)
            .get(function(req, res) {
                get(req, res);
            });
    }

    return {
        addRoutes: addRoutes
    }

})();
