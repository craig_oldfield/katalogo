module.exports = (function() {

    var mongo = require('mongodb');
    var oauth = require('../../oauth');
    var request = require('request').defaults({ encoding: null });

    var COLLECTION = "release",
        db,
        error,
        result;

    var initialise = function(mongo) {
        db = mongo;
    }

    var create = function(req, res) {

        console.log("**** NEW METHOD ****");
        var imageData;
        var createRelease = function(body, imageData) {
            var release = {
                userId: req.user._id,
                catalogNumber: req.body.catalogNumber,
                title: req.body.title,
                artist: req.body.artist,
                year: req.body.year,
                genre: req.body.genre,
                type: req.body.type,
                format: req.body.format,
                image: imageData
            };
            console.log("release with image = " + JSON.stringify(release));
            result = db.collection(COLLECTION).insert(
                release);
            _processWriteResult(res, result, release);
        }

        console.log("req.body  = " + JSON.stringify(req.body));

        if(req.body.imageurl) {
            console.log("requesting image data from " + req.body.imageurl);
            request.get({
                url: req.body.imageurl,
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:53.0) Gecko/20100101 Firefox/53.0'
                }
            }, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        console.log("image request successful");
                        imageData = mongo.Binary(new Buffer(body));
                        createRelease(req.body, imageData);
                    } else {
                        console.log("image request failed " + response.statusCode + " " + error);
                        createRelease(req.body);
                    }
                }
            );
        } else {
            createRelease(req.body);
        }
    }

    var get = function(req, res) {
        db.collection(COLLECTION).findOne({
            _id: new mongo.ObjectId(req.params.release_id),
            userId: req.user._id
        }, function(err, release) {
            if (err) {
                console.log(JSON.stringify(err));
                res.status(500).json(err);
            } else if (release) {
                res.status(200).json(release);
            } else {
                res.status(404).send();
            }
        });
    }

    var getImage = function(req, res) {
        db.collection(COLLECTION).findOne({
            _id: new mongo.ObjectId(req.params.release_id),
            userId: req.user._id
        }, function(err, release) {
            if (err) {
                console.log(JSON.stringify(err));
                res.status(500).json(err);
            } else if (release && release.image && release.image.length() > 0) {
                res.setHeader('content-type', 'image');
                res.status(200).send(release.image.read(0, release.image.length()));
            } else {
                res.setHeader('content-type', 'image/png');
                res.status(200).sendFile(__dirname + '/default.png');
            }
        });
    }

    var list = function(req, res) {

        var query = {
            userId: req.user._id
        };

        console.log("filterBy " + req.query.filterBy);
        if (req.query.keyword) {
            var regexpValue = '\.*' + req.query.keyword + '\.*';
            if (req.query.filterBy) {
                query[req.query.filterBy] = {$regex: regexpValue, $options : 'i'};
            } else {
                query['$or'] = [{
                        artist: {$regex: regexpValue, $options : 'i'}
                    },
                    {
                        title: {$regex: regexpValue, $options : 'i'}
                    },
                    {
                        genre: {$regex: regexpValue, $options : 'i'}
                    },
                    {
                        year: {$regex: regexpValue, $options : 'i'}
                    },
                    {
                        type: {$regex: regexpValue, $options : 'i'}
                    },
                    {
                        format: {$regex: regexpValue, $options : 'i'}
                    }
                ];
            }
        }

        console.log('query ' + JSON.stringify(query));

        db.collection(COLLECTION).find(query, { image: 0 }).sort([['artist', 1],['year', 1]]).toArray(function(err, releases) {
            if (err) {
                console.log(JSON.stringify(err));
                res.status(500).json(err);
            } else {
                res.status(200).json(releases);
            }
        });
    }

    var update = function(req, res) {

        console.log("Update");
        console.log(req.body);
        var release = {
            _id: new mongo.ObjectId(req.body._id),
            userId: req.user._id,
            catalogNumber: req.body.catalogNumber,
            title: req.body.title,
            artist: req.body.artist,
            year: req.body.year,
            genre: req.body.genre,
            type: req.body.type,
            format: req.body.format
        };
        db.collection(COLLECTION).update({
            _id: new mongo.ObjectId(req.body._id),
            userId: req.user._id
        }, release, function(err, count, status) {
            console.log("err = " + err);
            console.log("count = " + count);
            console.log("count.ok = " + JSON.parse(count).ok);
            console.log("status = " + status);
            if (err) {
                console.log(JSON.stringify(err));
                res.status(500).json(err);
            } else if (count && JSON.parse(count).ok > 0) {
                console.log("Updated " + count);
                res.status(200).json(release);
            } else {
                console.log("Not found");
                res.status(404).send();
            }
        });
    }

    var remove = function(req, res) {
        db.collection(COLLECTION).remove({
            _id: new mongo.ObjectId(req.params.release_id),
            userId: req.user._id
        }, function(err, release) {
            if (err) {
                console.log(JSON.stringify(err));
                res.status(500).json(err);
            } else {
                res.status(200).send();
            }
        });
    }

    var addRoutes = function(router) {
        router.route('/api/release')
            .all(oauth.ensureAuthenticated)
            .post(function(req, res) {
                create(req, res);
            })
            .get(function(req, res) {
                list(req, res);
            })
            .put(function(req, res) {
                update(req, res);
            });

        router.route('/api/release/image/:release_id')
            .all(oauth.ensureAuthenticated)
            .get(function(req, res) {
                getImage(req, res);
            });

        router.route('/api/release/:release_id')
            .all(oauth.ensureAuthenticated)
            .get(function(req, res) {
                get(req, res);
            })
            .delete(function(req, res) {
                remove(req, res);
            });
    }

    var _processWriteResult = function(res, writeResult, release) {
        var error;
        if (result.writeConcernError) error = result.writeConcernError;
        if (result.writeError) error = result.writeError;
        if (error) {
            res.status(500).json({
                status: 'fail',
                code: error.code,
                msg: error.errmsg
            });
        } else {
            res.status(201).json(release);
        }
    }

    return {
        addRoutes: addRoutes,
        initialise: initialise
    }


})();
