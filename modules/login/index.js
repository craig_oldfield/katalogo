module.exports = (function() {

    var template = require('./login.html');
    var parentNode;

    var initialise = function(parentNode) {
        parentNode.html(template());

        $('#facebook-login').click(function() {
            window.location = "/auth/facebook";
        });
    };

    return {
        initialise: initialise
    };

})();
