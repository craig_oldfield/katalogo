module.exports = (function () {

    var template = require('./top-10.html');

    var initialise = function (parent, title, field) {
        $.get({
            url:'/api/top-10/'+field,
        }).fail(_handleFail).done(function (top10) {
            var data = {
                title: title,
                entries: top10
            };
            parent.html(template(data));
        });
    }

    var _handleFail = function(data) {
        alert(JSON.stringify(data));
    }

    return {
        initialise: initialise
    }
})();
