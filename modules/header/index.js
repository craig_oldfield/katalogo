module.exports = (function () {

	var template = require('./header.html');

	var initialise = function (parentNode, user) {
        if(user.isAuthenticated) {
            parentNode.html(template(user));
            $('#new-release-btn').on('click', function () {
                $.publish('release.new');
            })
        }
	};

	return {
		initialise: initialise
	};

})();
